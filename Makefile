all: get_deps compile_all
dev: compile_self check_self tags
dev_start: dev start
STARTCMD = erl\
	-pa apps/gp/ebin\
	-pa deps/cowboy/ebin\
	-pa deps/cowlib/ebin\
	-pa deps/ebtree/ebin\
	-pa deps/ecoro/ebin\
	-pa deps/ees/ebin\
	-pa deps/eep/ebin\
	-pa deps/goldrush/ebin\
	-pa deps/jiffy/ebin\
	-pa deps/jiffy_v/ebin\
	-pa deps/lager/ebin\
	-pa deps/ranch/ebin\
	-s gp

clean:
	rebar clean

get_deps:
	rebar get-deps

compile_all:
	rebar compile

compile_self:
	rebar compile skip_deps=true

check_self:
	dialyzer -r -nn apps/gp/ebin

build_plt:
	dialyzer --build_plt --apps\
		erts\
		kernel\
		stdlib\
		deps/cowboy\
		deps/cowlib\
		deps/ebtree\
		deps/ecoro\
		deps/ees\
		deps/eep\
		deps/goldrush\
		deps/jiffy\
		deps/jiffy_v\
		deps/lager\
		deps/ranch

tags:
	ctags -R\
		apps/gp/src\
		deps/cowboy/src\
		deps/cowlib/src\
		deps/ebtree/src\
		deps/ecoro/src\
		deps/ees/src\
		deps/eep/src\
		deps/goldrush/src\
		deps/jiffy/src\
		deps/jiffy_v/src\
		deps/lager/src\
		deps/ranch/src

start:
	$(STARTCMD)

start_sasl:
	$(STARTCMD) -boot start_sasl

start_silent:
	$(STARTCMD) -detached
