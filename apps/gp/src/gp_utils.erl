-module(gp_utils).

%% public
-export([random_integer/1]).
-export([random_integer/2]).
-export([escape_html_chars/1]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% random_integer
%% ------------------------------------

-spec random_integer(integer()) -> integer().
random_integer(Max) ->
	random_integer(0, Max).

-spec random_integer(integer(), integer()) -> integer().
random_integer(Min, Max) when Min >= 0, Min =< Max ->
	random:uniform(Max - Min + 1) + Min - 1.

%% ------------------------------------
%% escape_html_chars
%% ------------------------------------

-spec escape_html_chars(binary()) -> binary().
escape_html_chars(Bin) ->
	<< <<(escape_html_char(B))/binary>> || <<B>> <= Bin >>.
escape_html_char($<) -> <<"&lt;">>;
escape_html_char($>) -> <<"&gt;">>;
escape_html_char($&) -> <<"&amp;">>;
escape_html_char(C)  -> <<C>>.
