-module(gp_es_spawn).

%% public
-export([init/1]).
-export([tick/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	ees:state()
) -> ees:state().

init(ES) ->
	ES.

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	integer(), ees:state()
) -> ees:state().

tick(Ticks, ES) ->
	spawn_process(Ticks, ES).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% spawn_process
%% ------------------------------------

-spec spawn_process(
	integer(), ees:state()
) -> ees:state().

spawn_process(Ticks, ES) ->
	{PianoEnt, Piano} = ees:get_component(piano, ES),
	#gp_piano_comp{
		pos = PianoPos,
		spawn_cooldown = SpawnCooldown
	} = Piano,
	NewES = case Ticks >= SpawnCooldown of
		true ->
			ZombieCount = dict:size(ees:get_components(zombie, ES)),
			PlayerCount = dict:size(ees:get_components(player, ES)),
			ES2 = case ZombieCount < PlayerCount * ?GP_ZOMBIE_MAX_COEF of
				true  -> spawn_zombie(PianoPos, ES);
				false -> ES
			end,
			ees:update_component(PianoEnt, fun(OtherPiano) ->
				OtherPiano#gp_piano_comp{
					spawn_cooldown = SpawnCooldown + spawn_cooldown(ES2)
				}
			end, piano, ES2);
		false ->
			ES
	end,
	NewES.

%% ------------------------------------
%% spawn_cooldown
%% ------------------------------------

-spec spawn_cooldown(
	ees:state()
) -> integer().

spawn_cooldown(ES) ->
	{_PianoEnt, Piano} = ees:get_component(piano, ES),
	#gp_piano_comp{pos = {PianoX, _PianoY}} = Piano,
	max(1, 20000 div ((max(0, PianoX) div 100) + 1)).

%% ------------------------------------
%% spawn_zombie
%% ------------------------------------

-spec spawn_zombie(
	gp_vec2(), ees:state()
) -> ees:state().

spawn_zombie({PianoX, PianoY}, ES) ->
	ZombieX = PianoX + ?GP_PIANO_SPAWN_OFFSET,
	ZombieY = PianoY + gp_utils:random_integer(?GP_PIANO_YOFFSET),
	{_Ent, NewES} = ees:create_entity([
		{zombie, #gp_zombie_comp{
			pos    = {ZombieX, ZombieY},
			view   = gp_utils:random_integer(0, 7),
			health = gp_utils:random_integer(0, ?GP_ZOMBIE_MAX_HEALTH) * ?GP_ZOMBIE_MAX_HEALTH_COEF}}
	], ES),
	NewES.
