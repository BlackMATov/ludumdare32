-module(gp).

-export([start/0]).
-export([stop/0]).
-export([profiler/1]).
-export([profiler/2]).

-define(APPS, [
	crypto, ranch, cowlib, cowboy,
	syntax_tools, compiler, goldrush, lager,
	gp
]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start
%% ------------------------------------

-spec start(
) -> ok.

start() ->
	lists:foldl(fun(App, AccIn) ->
		AccIn = application:start(App)
	end, ok, ?APPS).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	lists:foldr(fun(App, AccIn) ->
		AccIn = application:stop(App)
	end, ok, ?APPS).

%% ------------------------------------
%% profiler
%% ------------------------------------

-spec profiler(
	integer()
) -> ok.

profiler(Time) ->
	profiler(Time, "prof_file").

-spec profiler(
	integer(), string()
) -> ok.

profiler(Time, FileName) ->
	eep:start_file_tracing(FileName),
	timer:sleep(Time),
	eep:stop_tracing(),
	eep:convert_tracing(FileName).
