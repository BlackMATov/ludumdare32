-module(gp_world).
-behaviour(gen_server).

%% public
-export([start_link/0]).

%% public
-export([start_user/1]).
-export([stop_user/1]).
-export([user_action/2]).
-export([get_user_id/1]).
-export([get_all_users/0]).
-export([change_user_name/2]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("types.hrl").
-define(ES_SYSTEMS, [
	gp_es_broadmgr,
	gp_es_brain,
	gp_es_spawn
]).

-record(state, {
	users = sets:new() :: sets:set(pid()),
	ticks = 0          :: integer(),
	es    = ees:new()  :: ees:state()
}).
-type state() :: #state{}.

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% start_user
%% ------------------------------------

-spec start_user(
	pid()
) -> pid().

start_user(Socket) ->
	gen_server:call(?MODULE, {start_user, Socket}).

%% ------------------------------------
%% stop_user
%% ------------------------------------

-spec stop_user(
	pid()
) -> ok.

stop_user(User) ->
	gen_server:cast(?MODULE, {stop_user, User}).

%% ------------------------------------
%% user_action
%% ------------------------------------

-spec user_action(
	pid(), gp_action:state()
) -> ok.

user_action(User, Action) ->
	gen_server:cast(?MODULE, {user_action, User, Action}).

%% ------------------------------------
%% get_user_id
%% ------------------------------------

-spec get_user_id(
	pid()
) -> integer().

get_user_id(User) ->
	gen_server:call(?MODULE, {get_user_id, User}).

%% ------------------------------------
%% get_all_users
%% ------------------------------------

-spec get_all_users(
) -> list(pid()).

get_all_users() ->
	gen_server:call(?MODULE, get_all_users).

%% ------------------------------------
%% change_user_name
%% ------------------------------------

-spec change_user_name(
	pid(), binary()
) -> ok.

change_user_name(User, Name) ->
	gen_server:cast(?MODULE, {change_user_name, User, Name}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	list(term())
) -> {ok, state()}.

init([]) ->
	io:format("~p:init(Self:~p)~n", [?MODULE, self()]),
	{ok, stats(tick(create()))}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call({start_user, Socket}, _From, State) ->
	{User, NewState} = start_user_impl(Socket, State),
	{reply, User, NewState};

handle_call({get_user_id, Socket}, _From, State = #state{es = ES}) ->
	Id = get_user_id_impl(Socket, ES),
	{reply, Id, State};

handle_call(get_all_users, _From, State = #state{users = Users}) ->
	{reply, sets:to_list(Users), State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({stop_user, User}, State) ->
	NewState = stop_user_impl(User, State),
	{noreply, NewState};

handle_cast({user_action, User, Action}, State = #state{ticks = Ticks, es = ES}) ->
	NewAction = gp_action:add_opt(ticks, Ticks + ?GP_PLAYER_ACT_TIME, Action),
	NewES = user_action_impl(User, NewAction, ES),
	{noreply, State#state{es = NewES}};

handle_cast({change_user_name, User, Name}, State = #state{es = ES}) ->
	NewES = change_user_name_impl(User, Name, ES),
	{noreply, State#state{es = NewES}};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(stats, State) ->
	NewState = stats(State),
	{noreply, NewState};

handle_info(tick, State) ->
	NewState = tick(State),
	{noreply, NewState};

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(Reason, _State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% create
%% ------------------------------------

-spec create(
) -> state().

create() ->
	create_piano(#state{}).

%% ------------------------------------
%% stats
%% ------------------------------------

-spec stats(
	state()
) -> state().

stats(State = #state{users = Users, es = ES}) ->
	{_PianoEnt, Piano} = ees:get_component(piano, ES),
	#gp_piano_comp{pos = {PianoX, _PianoY}} = Piano,
	lager:info("Users: ~p, Progress: ~p~n", [sets:size(Users), PianoX]),
	erlang:send_after(?GP_STATS_INTERVAL, self(), stats),
	State.

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	state()
) -> state().

tick(State = #state{ticks = Ticks}) ->
	NewState = tick_systems(State),
	erlang:send_after(?GP_TICK_INTERVAL, self(), tick),
	NewState#state{ticks = Ticks + ?GP_TICK_INTERVAL}.

%% ------------------------------------
%% tick_systems
%% ------------------------------------

-spec tick_systems(
	state()
) -> state().

tick_systems(State = #state{ticks = Ticks, es = ES}) ->
	NewES = lists:foldl(fun(System, AccES) ->
		erlang:apply(System, tick, [Ticks, AccES])
	end, ES, ?ES_SYSTEMS),
	State#state{es = NewES}.

%% ------------------------------------
%% start_user_impl
%% ------------------------------------

-spec start_user_impl(
	pid(), state()
) -> {pid(), state()}.

start_user_impl(Socket,
	State = #state{users = Users, es = ES}
) ->
	{ok, User} = gp_user:start_link(Socket),
	NewES = create_player(User, ES),
	{User, State#state{
		users = sets:add_element(User, Users),
		es    = NewES
	}}.

%% ------------------------------------
%% stop_user_impl
%% ------------------------------------

-spec stop_user_impl(
	pid(), state()
) -> state().

stop_user_impl(User,
	State = #state{users = Users, es = ES}
) ->
	ok = gp_user:stop(User),
	NewES = destroy_player(User, ES),
	State#state{
		users = sets:del_element(User, Users),
		es    = NewES
	}.

%% ------------------------------------
%% user_action_impl
%% ------------------------------------

-spec user_action_impl(
	pid(), gp_action:state(), ees:state()
) -> ees:state().

user_action_impl(User, Action, ES) ->
	dict:fold(fun(Ent, Player, AccES) ->
		#gp_player_comp{user = OtherUser} = Player,
		case OtherUser of
			User ->
				ees:update_component(Ent, fun(OtherPlayer) ->
					OtherPlayer#gp_player_comp{action = Action}
				end, player, AccES);
			_ -> AccES
		end
	end, ES, ees:get_components(player, ES)).

%% ------------------------------------
%% get_user_id_impl
%% ------------------------------------

-spec get_user_id_impl(
	pid(), ees:state()
) -> integer().

get_user_id_impl(User, ES) ->
	dict:fold(fun(Ent, Player, AccId) ->
		#gp_player_comp{user = OtherUser} = Player,
		case OtherUser of
			User -> Ent;
			_ -> AccId
		end
	end, -1, ees:get_components(player, ES)).

%% ------------------------------------
%% change_user_name_impl
%% ------------------------------------

-spec change_user_name_impl(
	pid(), binary(), ees:state()
) -> ees:state().

change_user_name_impl(User, Name, ES) ->
	case get_user_id_impl(User, ES) of
		-1 -> ES;
		Ent ->
			lager:info("User change name. Id:~p, Name:~ts~n", [Ent, Name]),
			ees:update_component(Ent, fun(Player) ->
				Player#gp_player_comp{name = Name}
			end, player, ES)
	end.

%% ------------------------------------
%% create_player
%% ------------------------------------

-spec create_player(
	pid(), ees:state()
) -> ees:state().

create_player(User, ES) ->
	PlayerCount = dict:size(ees:get_components(player, ES)),
	lager:info("Player online. Count:~p~n", [PlayerCount + 1]),
	{_PianoEnt, Piano} = ees:get_component(piano, ES),
	#gp_piano_comp{pos = {PianoX, PianoY}} = Piano,
	PlayerX = max(?GP_PLAYER_SPAWN_MIN, PianoX - ?GP_PIANO_XOFFSET),
	PlayerY = PianoY + gp_utils:random_integer(?GP_PIANO_YOFFSET),
	{_Ent, NewES} = ees:create_entity([
		{player, #gp_player_comp{
			pos = {PlayerX, PlayerY},
			view = gp_utils:random_integer(0, 21),
			user = User}}
	], ES),
	NewES.

%% ------------------------------------
%% destroy_player
%% ------------------------------------

-spec destroy_player(
	pid(), ees:state()
) -> ees:state().

destroy_player(User, ES) ->
	PlayerCount = dict:size(ees:get_components(player, ES)),
	lager:info("Player offline. Count:~p~n", [PlayerCount - 1]),
	dict:fold(fun(Ent, Player, AccES) ->
		#gp_player_comp{user = OtherUser} = Player,
		case OtherUser of
			User -> ees:remove_entity(Ent, AccES);
			_ -> AccES
		end
	end, ES, ees:get_components(player, ES)).

%% ------------------------------------
%% create_piano
%% ------------------------------------

-spec create_piano(
	state()
) -> state().

create_piano(State = #state{es = ES}) ->
	{_Ent, NewES} = ees:create_entity([
		{piano, #gp_piano_comp{pos = {150,0}}}
	], ES),
	State#state{es = NewES}.
