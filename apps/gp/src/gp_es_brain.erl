-module(gp_es_brain).

%% public
-export([init/1]).
-export([tick/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	ees:state()
) -> ees:state().

init(ES) ->
	ES.

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	integer(), ees:state()
) -> ees:state().

tick(Ticks, ES) ->
	pianos_process(
		Ticks,
		actions_process(
			Ticks,
			players_process(
				Ticks,
				zombies_process(Ticks, ES)))).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% pianos_process
%% ------------------------------------

-spec pianos_process(
	integer(), ees:state()
) -> ees:state().

pianos_process(Ticks, ES) ->
	dict:fold(fun(Ent, _Piano, AccES) ->
		piano_process(Ent, Ticks, AccES)
	end, ES, ees:get_components(piano, ES)).

%% ------------------------------------
%% piano_process
%% ------------------------------------

-spec piano_process(
	gp_entity(), integer(), ees:state()
) -> ees:state().

piano_process(Ent, Ticks, ES) ->
	Piano = ees:get_component(Ent, piano, ES),
	#gp_piano_comp{
		impulse          = Impulse,
		impulse_cooldown = ImpulseCooldown
	} = Piano,
	case ImpulseCooldown < Ticks of
		true -> move_piano(Ent, Impulse, Ticks, ES);
		false -> ES
	end.

%% ------------------------------------
%% zombies_process
%% ------------------------------------

-spec zombies_process(
	integer(), ees:state()
) -> ees:state().

zombies_process(Ticks, ES) ->
	NewES = dict:fold(fun(Ent, _Zombie, AccES) ->
		zombie_static_impulse(Ent, zombie_process(Ent, Ticks, AccES))
	end, ES, ees:get_components(zombie, ES)),
	dict:fold(fun(Ent, Zombie, AccES) ->
		#gp_zombie_comp{health = Health} = Zombie,
		case Health =< 0.0 of
			true  ->
				ees:remove_entity(Ent, add_frag(AccES));
			false -> AccES
		end
	end, NewES, ees:get_components(zombie, NewES)).

%% ------------------------------------
%% zombie_process
%% ------------------------------------

-spec zombie_process(
	gp_entity(), integer(), ees:state()
) -> ees:state().

zombie_process(Ent, _Ticks, ES) ->
	ees:update_component(Ent, fun(Zombie) ->
		#gp_zombie_comp{pos = {X,Y}} = Zombie,
		MaxPianoX = max_piano_x(ES) + (Ent rem (?GP_PIANO_PUSH_ZONE - 1)),
		NewX = max(MaxPianoX, X - ?GP_ZOMBIE_SPEED),
		Zombie#gp_zombie_comp{pos = {NewX,Y}}
	end, zombie, ES).

%% ------------------------------------
%% zombie_static_impulse
%% ------------------------------------

-spec zombie_static_impulse(
	gp_entity(), ees:state()
) -> ees:state().

zombie_static_impulse(Ent, ES) ->
	case is_zombie_near_piano(Ent, ES) of
		true ->
			add_impulse_piano(-?GP_IMPULSE_ZOMBIE_STATIC, ES);
		false ->
			ES
	end.

%% ------------------------------------
%% players_process
%% ------------------------------------

-spec players_process(
	integer(), ees:state()
) -> ees:state().

players_process(_Ticks, ES) ->
	dict:fold(fun(Ent, _Zombie, AccES) ->
		player_static_impulse(Ent, AccES)
	end, ES, ees:get_components(player, ES)).

%% ------------------------------------
%% actions_process
%% ------------------------------------

-spec actions_process(
	integer(), ees:state()
) -> ees:state().

actions_process(Ticks, ES) ->
	dict:fold(fun(Ent, Player, AccES) ->
		#gp_player_comp{action = Action} = Player,
		case Action of
			undefined ->
				AccES;
			Action ->
				ActionType = gp_action:get_type(Action),
				action_process(Ent, ActionType, Action, Ticks, AccES)
		end
	end, ES, ees:get_components(player, ES)).

%% ------------------------------------
%% action_process
%% ------------------------------------

-spec action_process(
	gp_entity(), gp_action:type(), gp_action:state(), integer(), ees:state()
) -> ees:state().

action_process(Ent, action_left, Action, Ticks, AccES) ->
	ees:update_component(Ent, fun(Player) ->
		#gp_player_comp{pos = {X,Y}} = Player,
		NewX = max(?GP_PLAYER_SPAWN_MIN, X - ?GP_PLAYER_SPEED),
		Player#gp_player_comp{
			pos    = {NewX, Y},
			dir    = -1,
			action = action_after_process(Ticks, Action)}
	end, player, AccES);

action_process(Ent, action_right, Action, Ticks, AccES) ->
	ees:update_component(Ent, fun(Player) ->
		#gp_player_comp{pos = {X,Y}} = Player,
		NewX = min(min_piano_x(AccES), X + ?GP_PLAYER_SPEED),
		Player#gp_player_comp{
			pos    = {NewX, Y},
			dir    = 1,
			action = action_after_process(Ticks, Action)}
	end, player, AccES);

action_process(Ent, action_top, _Action, Ticks, AccES) ->
	NewAccES = case is_player_near_piano(Ent, AccES) of
		true ->
			{PianoEnt, Piano} = ees:get_component(piano, AccES),
			#gp_piano_comp{cooldown = Cooldown} = Piano,
			case Cooldown < Ticks of
				true ->
					AccES2 = piano_top_process(Ent, AccES),
					ees:update_component(PianoEnt, fun(OtherPiano) ->
						OtherPiano#gp_piano_comp{
							sound     = gp_utils:random_integer(0, 2),
							sound_rnd = gp_utils:random_integer(0, 1000),
							cooldown  = Ticks + ?GP_PIANO_COOLDOWN
						}
					end, piano, AccES2);
				false -> AccES
			end;
		false ->
			AccES
	end,
	ees:update_component(Ent, fun(Player) ->
		Player#gp_player_comp{action = undefined}
	end, player, NewAccES);

action_process(Ent, action_push, Action, _Ticks, AccES) ->
	NewAccES = case is_player_near_piano(Ent, AccES) of
		true ->
			add_impulse_piano(
				gp_action:get_opt(Action, coef), AccES);
		false ->
			AccES
	end,
	ees:update_component(Ent, fun(Player) ->
		Player#gp_player_comp{action = undefined}
	end, player, NewAccES).

%% ------------------------------------
%% action_after_process
%% ------------------------------------

-spec action_after_process(
	integer(), gp_action:state()
) -> gp_action:state() | undefined.

action_after_process(Ticks, Action) ->
	ActTicks = gp_action:get_opt(Action, ticks, Ticks),
	case ActTicks =< Ticks of
		true  -> undefined;
		false -> Action
	end.

%% ------------------------------------
%% player_static_impulse
%% ------------------------------------

-spec player_static_impulse(
	gp_entity(), ees:state()
) -> ees:state().

player_static_impulse(Ent, ES) ->
	case is_player_near_piano(Ent, ES) of
		true ->
			{_PianoEnt, Piano} = ees:get_component(piano, ES),
			#gp_piano_comp{impulse = PianoImpulse} = Piano,
			case PianoImpulse < 0.0 of
				true ->
					AddImpulse = min(?GP_IMPULSE_PLAYER_STATIC, -PianoImpulse),
					add_impulse_piano(AddImpulse, ES);
				false ->
					ES
			end;
		false ->
			ES
	end.

%% ------------------------------------
%% is_player_near_piano
%% ------------------------------------

-spec is_player_near_piano(
	gp_entity(), ees:state()
) -> boolean().

is_player_near_piano(PlayerEnt, ES) ->
	is_player_near_piano(PlayerEnt, min_piano_x(ES), ES).

-spec is_player_near_piano(
	gp_entity(), integer(), ees:state()
) -> boolean().

is_player_near_piano(PlayerEnt, PianoX, ES) ->
	Player = ees:get_component(PlayerEnt, player, ES),
	#gp_player_comp{pos = {X,_Y}} = Player,
	PianoX < X + ?GP_PIANO_PUSH_ZONE.

%% ------------------------------------
%% is_zombie_near_piano
%% ------------------------------------

-spec is_zombie_near_piano(
	gp_entity(), ees:state()
) -> boolean().

is_zombie_near_piano(ZombieEnt, ES) ->
	is_zombie_near_piano(ZombieEnt, min_piano_x(ES), ES).

-spec is_zombie_near_piano(
	gp_entity(), integer(), ees:state()
) -> boolean().

is_zombie_near_piano(ZombieEnt, PianoX, ES) ->
	Zombie = ees:get_component(ZombieEnt, zombie, ES),
	#gp_zombie_comp{pos = {X,_Y}} = Zombie,
	PianoX + ?GP_PIANO_WIDTH > X - ?GP_PIANO_PUSH_ZONE.

%% ------------------------------------
%% min_piano_x
%% ------------------------------------

-spec min_piano_x(
	ees:state()
) -> integer().

min_piano_x(ES) ->
	{_PianoEnt, Piano} = ees:get_component(piano, ES),
	#gp_piano_comp{pos = {X,_Y}} = Piano,
	X.

%% ------------------------------------
%% max_piano_x
%% ------------------------------------

-spec max_piano_x(
	ees:state()
) -> integer().

max_piano_x(ES) ->
	min_piano_x(ES) + ?GP_PIANO_WIDTH.

%% ------------------------------------
%% move_piano
%% ------------------------------------

-spec move_piano(
	gp_entity(), float(), integer(), ees:state()
) -> ees:state().

move_piano(Ent, Impulse, Ticks, ES) ->
	{{PianoX, DeltaX}, NewES} = ees:rupdate_component(Ent, fun(Piano) ->
		#gp_piano_comp{pos = {X,Y}} = Piano,
		NewX = max(?GP_PLAYER_SPAWN_MIN * 2, trunc(X + Impulse)),
		{Piano#gp_piano_comp{
			pos              = {NewX, Y},
			impulse          = 0.0,
			impulse_cooldown = Ticks + ?GP_IMPULSE_COOLDOWN
		}, {X, NewX - X}}
	end, piano, ES),
	NewES2 = case DeltaX < 0 of
		true  -> move_players_near_piano(DeltaX, PianoX, NewES);
		false -> move_zombies_near_piano(DeltaX, PianoX, NewES)
	end,
	score_players_near_piano(max(0, DeltaX), NewES2).

%% ------------------------------------
%% add_impulse_piano
%% ------------------------------------

-spec add_impulse_piano(
	float(), ees:state()
) -> ees:state().

add_impulse_piano(Impulse, ES) ->
	{PianoEnt, _Piano} = ees:get_component(piano, ES),
	ees:update_component(PianoEnt, fun(Piano) ->
		#gp_piano_comp{impulse = PianoImpulse} = Piano,
		NewImpulse = max(-?GP_IMPULSE_MAX,
			min(?GP_IMPULSE_MAX, PianoImpulse + Impulse)),
		%io:format("Impulse: ~p~n", [NewImpulse]),
		Piano#gp_piano_comp{impulse = NewImpulse}
	end, piano, ES).

%% ------------------------------------
%% piano_top_process
%% ------------------------------------

-spec piano_top_process(
	gp_entity(), ees:state()
) -> ees:state().

piano_top_process(PlayerEnt, ES) ->
	ZombiesNearPianos = zombies_near_piano(ES),
	ZombiesNearPianosLen = length(ZombiesNearPianos),
	case ZombiesNearPianosLen > 0 of
		true ->
			Num = max(1, trunc(ZombiesNearPianosLen * ?GP_PIANO_KILLS_COEF)),
			ES2 = kill_zombies(ZombiesNearPianos, Num, ES),
			score_player(PlayerEnt, Num * ?GP_PLAYER_FRAG_SCORE, ES2);
		false -> ES
	end.

%% ------------------------------------
%% kill_zombies
%% ------------------------------------

-spec kill_zombies(
	list(gp_entity()), integer(), ees:state()
) -> ees:state().

kill_zombies(_, 0, ES) ->
	ES;

kill_zombies([], _, ES) ->
	ES;

kill_zombies([Ent | Tail], Num, ES) ->
	NewES = ees:remove_entity(Ent, add_frag(ES)),
	kill_zombies(Tail, Num - 1, NewES).

%% ------------------------------------
%% add_frag
%% ------------------------------------

-spec add_frag(
	ees:state()
) -> ees:state().

add_frag(ES) ->
	{PianoEnt, _Piano} = ees:get_component(piano, ES),
	ees:update_component(PianoEnt, fun(Piano) ->
		#gp_piano_comp{frags = Frags} = Piano,
		Piano#gp_piano_comp{frags = Frags + 1}
	end, piano, ES).

%% ------------------------------------
%% zombies_near_piano
%% ------------------------------------

-spec zombies_near_piano(
	ees:state()
) -> list(gp_entity()).

zombies_near_piano(ES) ->
	PianoX = min_piano_x(ES) + ?GP_PIANO_WIDTH + ?GP_PIANO_PUSH_ZONE,
	dict:fold(fun(ZombieEnt, Zombie, AccZombies) ->
		#gp_zombie_comp{pos = {ZombieX, _ZombieY}} = Zombie,
		case ZombieX < PianoX of
			true -> [ZombieEnt | AccZombies];
			false -> AccZombies
		end
	end, [], ees:get_components(zombie, ES)).

%% ------------------------------------
%% move_players_near_piano
%% ------------------------------------

-spec move_players_near_piano(
	float(), integer(), ees:state()
) -> ees:state().

move_players_near_piano(Impulse, PianoX, ES) ->
	dict:fold(fun(PlayerEnt, _Player, AccES) ->
		case is_player_near_piano(PlayerEnt, PianoX, AccES) of
			true ->
				ees:update_component(PlayerEnt, fun(Player) ->
					#gp_player_comp{pos = {X,Y}} = Player,
					Player#gp_player_comp{pos = {trunc(X + Impulse), Y}}
				end, player, AccES);
			false ->
				AccES
		end
	end, ES, ees:get_components(player, ES)).

%% ------------------------------------
%% move_zombies_near_piano
%% ------------------------------------

-spec move_zombies_near_piano(
	float(), integer(), ees:state()
) -> ees:state().

move_zombies_near_piano(Impulse, PianoX, ES) ->
	dict:fold(fun(ZombieEnt, _Zombie, AccES) ->
		case is_zombie_near_piano(ZombieEnt, PianoX, AccES) of
			true ->
				ees:update_component(ZombieEnt, fun(Zombie) ->
					#gp_zombie_comp{
						pos    = {X,Y},
						health = Health
					} = Zombie,
					Zombie#gp_zombie_comp{
						pos    = {trunc(X + Impulse), Y},
						health = Health - Impulse}
				end, zombie, AccES);
			false ->
				AccES
		end
	end, ES, ees:get_components(zombie, ES)).

%% ------------------------------------
%% score_player
%% ------------------------------------

-spec score_player(
	gp_entity(), integer(), ees:state()
) -> ees:state().

score_player(PlayerEnt, AddScore, ES) ->
	ees:update_component(PlayerEnt, fun(Player) ->
		#gp_player_comp{score = Score} = Player,
		Player#gp_player_comp{score = Score + AddScore}
	end, player, ES).

%% ------------------------------------
%% score_players_near_piano
%% ------------------------------------

-spec score_players_near_piano(
	integer(), ees:state()
) -> ees:state().

score_players_near_piano(AddScore, ES) ->
	dict:fold(fun(PlayerEnt, _Player, AccES) ->
		case is_player_near_piano(PlayerEnt, AccES) of
			true  -> score_player(PlayerEnt, AddScore, AccES);
			false -> AccES
		end
	end, ES, ees:get_components(player, ES)).
