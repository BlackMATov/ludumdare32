-module(gp_socket).
-behaviour(cowboy_websocket_handler).

%% public
-export([send_text/2]).
-export([send_binary/2]).

%% cowboy_websocket_handler
-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% send_text
%% ------------------------------------

-spec send_text(
	pid(), binary()
) -> ok.

send_text(Pid, Msg) ->
	Pid ! {send_text, Msg},
	ok.

%% ------------------------------------
%% send_binary
%% ------------------------------------

-spec send_binary(
	pid(), binary()
) -> ok.

send_binary(Pid, Msg) ->
	Pid ! {send_binary, Msg},
	ok.

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR cowboy_websocket_handler
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init({tcp, http}, _Req, _Opts) ->
	{upgrade, protocol, cowboy_websocket}.

%% ------------------------------------
%% websocket_init
%% ------------------------------------

websocket_init(_TransportName, Req, _Opts) ->
	io:format("~p:init(Self:~p)~n", [?MODULE, self()]),
	User = gp_world:start_user(self()),
	{ok, Req, User}.

%% ------------------------------------
%% websocket_handle
%% ------------------------------------

websocket_handle({text, Msg}, Req, State) ->
	gp_user:text_msg_handle(State, Msg),
	{ok, Req, State};

websocket_handle({binary, Msg}, Req, State) ->
	gp_user:binary_msg_handle(State, Msg),
	{ok, Req, State};

websocket_handle(_Data, Req, State) ->
	{ok, Req, State}.

%% ------------------------------------
%% websocket_info
%% ------------------------------------

websocket_info({send_text, Msg}, Req, State) ->
	Info = {text, Msg},
	{reply, Info, Req, State};

websocket_info({send_binary, Msg}, Req, State) ->
	Info = {binary, Msg},
	{reply, Info, Req, State};

websocket_info(_Info, Req, State) ->
	{ok, Req, State}.

%% ------------------------------------
%% websocket_terminate
%% ------------------------------------

websocket_terminate(Reason, _Req, State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok = gp_world:stop_user(State).
