-module(gp_es_broadmgr).

%% public
-export([init/1]).
-export([tick/2]).

-include("types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	ees:state()
) -> ees:state().

init(ES) ->
	ES.

%% ------------------------------------
%% tick
%% ------------------------------------

-spec tick(
	integer(), ees:state()
) -> ees:state().

tick(Ticks, ES) ->
	ok = broadcast(Ticks, ES),
	ES.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% broadcast
%% ------------------------------------

-spec broadcast(
	integer(), ees:state()
) -> ok.

broadcast(Ticks, ES) ->
	Text = jiffy:encode([<<"world_info">>, [
		players_info(ES),
		zombies_info(ES),
		pianos_info(ES),
		Ticks
	]]),
	%io:format("Text: ~p~n", [Text]),
	lists:foreach(fun(User) ->
		gp_user:send_text(User, Text)
	end, all_users(ES)).

%% ------------------------------------
%% all_users
%% ------------------------------------

-spec all_users(
	ees:state()
) -> list(pid()).

all_users(ES) ->
	dict:fold(fun(_PlayerEnt, Player, AccUsers) ->
		#gp_player_comp{user = User} = Player,
		[User | AccUsers]
	end, [], ees:get_components(player, ES)).

%% ------------------------------------
%% players_info
%% ------------------------------------

-spec players_info(
	ees:state()
) -> gp_json().

players_info(ES) ->
	dict:fold(fun(Ent, Player, AccInfo) ->
		[player_info(Ent, Player) | AccInfo]
	end, [], ees:get_components(player, ES)).

%% ------------------------------------
%% zombies_info
%% ------------------------------------

-spec zombies_info(
	ees:state()
) -> gp_json().

zombies_info(ES) ->
	dict:fold(fun(Ent, Zombie, AccInfo) ->
		[zombie_info(Ent, Zombie) | AccInfo]
	end, [], ees:get_components(zombie, ES)).

%% ------------------------------------
%% pianos_info
%% ------------------------------------

-spec pianos_info(
	ees:state()
) -> gp_json().

pianos_info(ES) ->
	dict:fold(fun(Ent, Piano, AccInfo) ->
		[piano_info(Ent, Piano) | AccInfo]
	end, [], ees:get_components(piano, ES)).

%% ------------------------------------
%% player_info
%% ------------------------------------

-spec player_info(
	gp_entity(), gp_player_comp()
) -> gp_json().

player_info(Ent,
	#gp_player_comp{
		pos   = Pos,
		dir   = Dir,
		name  = Name,
		view  = View,
		score = Score
	}
) ->
	[
		Ent,
		vec2_json(Pos),
		Dir,
		Name,
		View,
		Score
	].

%% ------------------------------------
%% piano_info
%% ------------------------------------

-spec piano_info(
	gp_entity(), gp_piano_comp()
) -> gp_json().

piano_info(Ent,
	#gp_piano_comp{
		pos       = Pos,
		sound     = Sound,
		sound_rnd = SoundRnd,
		cooldown  = Cooldown,
		frags     = Frags
	}
) ->
	[
		Ent,
		vec2_json(Pos),
		Sound,
		SoundRnd,
		Cooldown,
		Frags
	].

%% ------------------------------------
%% zombie_info
%% ------------------------------------

-spec zombie_info(
	gp_entity(), gp_zombie_comp()
) -> gp_json().

zombie_info(Ent,
	#gp_zombie_comp{
		pos  = Pos,
		view = View
	}
) ->
	[
		Ent,
		vec2_json(Pos),
		View
	].

%% ------------------------------------
%% vec2_json
%% ------------------------------------

-spec vec2_json(
	gp_vec2()
) -> gp_json().

vec2_json({X,Y}) -> [X,Y].
