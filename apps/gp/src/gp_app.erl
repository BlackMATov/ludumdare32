-module(gp_app).
-behaviour(application).

%% application
-export([start/2]).
-export([stop/1]).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR application
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start
%% ------------------------------------

start(_Type, _Args) ->
	{ok, Port}      = application:get_env(port),
	{ok, Acceptors} = application:get_env(acceptors),
	Dispatch = cowboy_router:compile([
		{'_', [
			{"/websocket", gp_socket,     []},
			{"/",          cowboy_static, {priv_file, gp, "client/index.html"}},
			{"/[...]",     cowboy_static, {priv_dir,  gp, "client"}}
		]}
	]),
	{ok, _} = cowboy:start_http(http_listener, Acceptors,
		[{port, Port}],
		[{env, [{dispatch, Dispatch}]}]
	),
	gp_app_sup:start_link().

%% ------------------------------------
%% stop
%% ------------------------------------

stop(_State) ->
	ok.
