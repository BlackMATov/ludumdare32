-module(gp_action).

%% public
-export([new/1]).
-export([new/2]).
-export([add_opt/3]).
-export([get_opt/2]).
-export([get_opt/3]).
-export([get_type/1]).

-include("types.hrl").

-type type() ::
	action_left |
	action_right |
	action_top |
	action_push.

-record(state, {
	type      :: type(),
	opts = [] :: list({atom(), term()})
}).
-type state() :: #state{}.

-export_type([type/0, state/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% new
%% ------------------------------------

-spec new(
	type()
) -> state().

new(Type) ->
	#state{type = Type}.

-spec new(
	type(), list({atom(), term()})
) -> state().

new(Type, Opts) ->
	#state{type = Type, opts = Opts}.

%% ------------------------------------
%% add_opt
%% ------------------------------------

-spec add_opt(
	atom(), term(), state()
) -> state().

add_opt(Opt, Value, State = #state{opts = Opts}) ->
	State#state{opts = [{Opt, Value} | Opts]}.

%% ------------------------------------
%% get_opt
%% ------------------------------------

-spec get_opt(
	state(), atom()
) -> term().

get_opt(#state{opts = Opts}, Opt) ->
	proplists:get_value(Opt, Opts).

-spec get_opt(
	state(), atom(), term()
) -> term().

get_opt(#state{opts = Opts}, Opt, Def) ->
	proplists:get_value(Opt, Opts, Def).

%% ------------------------------------
%% get_type
%% ------------------------------------

-spec get_type(
	state()
) -> type().

get_type(#state{type = Type}) ->
	Type.
