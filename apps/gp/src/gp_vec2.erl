-module(gp_vec2).

-export([neg/1, add/2, sub/2, mul/2]).
-export([div_float/2, div_trunc/2, div_round/2]).
-export([len_sqr/1, len_float/1, len_trunc/1, len_round/1]).
-export([dist_sqr/2, dist_float/2, dist_trunc/2, dist_round/2]).

-type vec2() :: {integer(), integer()}.
-export_type([vec2/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% neg, add, sub, mul
%% ------------------------------------

-spec neg(vec2()) -> vec2().
neg({X, Y}) ->
	{-X, -Y}.

-spec add(vec2(), vec2() | integer()) -> vec2().
add({X1, Y1}, {X2, Y2}) ->
	{X1 + X2, Y1 + Y2};
add(XY, V) ->
	add(XY, {V, V}).

-spec sub(vec2(), vec2() | integer()) -> vec2().
sub({X1, Y1}, {X2, Y2}) ->
	{X1 - X2, Y1 - Y2};
sub(XY, V) ->
	sub(XY, {V, V}).

-spec mul(vec2(), vec2() | integer()) -> vec2().
mul({X1, Y1}, {X2, Y2}) ->
	{X1 * X2, Y1 * Y2};
mul(XY, V) ->
	mul(XY, {V, V}).

%% ------------------------------------
%% div_XXX
%% ------------------------------------

-spec div_float(vec2(), vec2() | integer()) -> {float(), float()}.
div_float({X1, Y1}, {X2, Y2}) ->
	{X1 / X2, Y1 / Y2};
div_float(XY, V) ->
	div_float(XY, {V, V}).

-spec div_trunc(vec2(), vec2() | integer()) -> vec2().
div_trunc(V1, V2) ->
	{X, Y} = div_float(V1, V2),
	{trunc(X), trunc(Y)}.

-spec div_round(vec2(), vec2() | integer()) -> vec2().
div_round(V1, V2) ->
	{X, Y} = div_float(V1, V2),
	{round(X), round(Y)}.

%% ------------------------------------
%% len_XXX
%% ------------------------------------

-spec len_sqr(vec2()) -> integer().
len_sqr({X, Y}) ->
	X * X + Y * Y.

-spec len_float(vec2()) -> float().
len_float(XY) ->
	math:sqrt(len_sqr(XY)).

-spec len_trunc(vec2()) -> integer().
len_trunc(XY) ->
	trunc(len_float(XY)).

-spec len_round(vec2()) -> integer().
len_round(XY) ->
	round(len_float(XY)).

%% ------------------------------------
%% dist_XXX
%% ------------------------------------

-spec dist_sqr(vec2(), vec2()) -> integer().
dist_sqr(V1, V2) ->
	len_sqr(sub(V2, V1)).

-spec dist_float(vec2(), vec2()) -> float().
dist_float(V1, V2) ->
	len_float(sub(V2, V1)).

-spec dist_trunc(vec2(), vec2()) -> integer().
dist_trunc(V1, V2) ->
	len_trunc(sub(V2, V1)).

-spec dist_round(vec2(), vec2()) -> integer().
dist_round(V1, V2) ->
	len_round(sub(V2, V1)).
