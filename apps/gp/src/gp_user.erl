-module(gp_user).
-behaviour(gen_server).

%% public
-export([start_link/1]).
-export([stop/1]).

%% public
-export([send_msg/2]).
-export([send_json/2]).
-export([send_json/3]).
-export([send_text/2]).
-export([send_binary/2]).

%% public
-export([text_msg_handle/2]).
-export([binary_msg_handle/2]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("types.hrl").

-record(state, {
	socket = undefined :: pid()
}).
-type state() :: #state{}.

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
	pid()
) -> {ok, pid()}.

start_link(Socket) ->
	gen_server:start_link(?MODULE, [Socket], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
	pid()
) -> ok.

stop(Pid) ->
	gen_server:cast(Pid, stop).

%% ------------------------------------
%% send_msg
%% ------------------------------------

-spec send_msg(
	pid(), binary()
) -> ok.

send_msg(Pid, MsgId) ->
	Msg = [MsgId,[]],
	gen_server:cast(Pid, {send_json, Msg}).

%% ------------------------------------
%% send_json
%% ------------------------------------

-spec send_json(
	pid(), gp_json()
) -> ok.

send_json(Pid, Msg) ->
	gen_server:cast(Pid, {send_json, Msg}).

-spec send_json(
	pid(), binary(), gp_json()
) -> ok.

send_json(Pid, MsgId, MsgValue) ->
	Msg = [MsgId, MsgValue],
	gen_server:cast(Pid, {send_json, Msg}).

%% ------------------------------------
%% send_text
%% ------------------------------------

-spec send_text(
	pid(), binary()
) -> ok.

send_text(Pid, Msg) ->
	gen_server:cast(Pid, {send_text, Msg}).

%% ------------------------------------
%% send_binary
%% ------------------------------------

-spec send_binary(
	pid(), binary()
) -> ok.

send_binary(Pid, Msg) ->
	gen_server:cast(Pid, {send_binary, Msg}).

%% ------------------------------------
%% text_msg_handle
%% ------------------------------------

-spec text_msg_handle(
	pid(), binary()
) -> ok.

text_msg_handle(Pid, Msg) ->
	gen_server:cast(Pid, {text_msg_handle, Msg}).

%% ------------------------------------
%% binary_msg_handle
%% ------------------------------------

-spec binary_msg_handle(
	pid(), binary()
) -> ok.

binary_msg_handle(Pid, Msg) ->
	gen_server:cast(Pid, {binary_msg_handle, Msg}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

-spec init(
	list(term())
) -> {ok, state()}.

init([Socket]) ->
	io:format("~p:init(Self:~p, Socket:~p)~n", [?MODULE, self(), Socket]),
	{ok, #state{socket = Socket}}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast(stop, State) ->
	io:format("~p:stop(Self:~p)~n", [?MODULE, self()]),
	{stop, normal, State};

handle_cast({send_json, Msg},
	State = #state{socket = Socket}
) ->
	ok = gp_socket:send_text(Socket, jiffy:encode(Msg)),
	{noreply, State};

handle_cast({send_text, Msg},
	State = #state{socket = Socket}
) ->
	ok = gp_socket:send_text(Socket, Msg),
	{noreply, State};

handle_cast({send_binary, Msg},
	State = #state{socket = Socket}
) ->
	ok = gp_socket:send_binary(Socket, Msg),
	{noreply, State};

handle_cast({text_msg_handle, Msg}, State) ->
	ok = json_msg_handle_impl(jiffy:decode(Msg), State),
	{noreply, State};

handle_cast({binary_msg_handle, Msg}, State) ->
	ok = binary_msg_handle_impl(Msg),
	{noreply, State};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(Reason, _State) ->
	io:format(
		"~p:terminate(Self:~p, Reason:~p)~n",
		[?MODULE, self(), Reason]),
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% json_msg_handle_impl
%% ------------------------------------

-spec json_msg_handle_impl(
	gp_json(), state()
) -> ok.

json_msg_handle_impl([<<"request_ping">>, MsgValue], _State) ->
	ok = send_json(self(), <<"ping_info">>, MsgValue);

json_msg_handle_impl([<<"request_id">>, _MsgValue], _State) ->
	Id = gp_world:get_user_id(self()),
	ok = send_json(self(), <<"id_info">>, [Id]);

json_msg_handle_impl([<<"change_name">>, [Name]], _State) when is_binary(Name) ->
	EscapeName = gp_utils:escape_html_chars(Name),
	gp_world:change_user_name(self(), EscapeName);

json_msg_handle_impl([<<"say">>, [Msg]], _State) when is_binary(Msg) ->
	EscapeMsg = gp_utils:escape_html_chars(Msg),
	say_impl(self(), EscapeMsg);

json_msg_handle_impl([<<"action_left">>, _MsgValue], _State) ->
	Action = gp_action:new(action_left),
	gp_world:user_action(self(), Action);

json_msg_handle_impl([<<"action_right">>, _MsgValue], _State) ->
	Action = gp_action:new(action_right),
	gp_world:user_action(self(), Action);

json_msg_handle_impl([<<"action_top">>, _MsgValue], _State) ->
	Action = gp_action:new(action_top),
	gp_world:user_action(self(), Action);

json_msg_handle_impl([<<"action_push">>, [Coef]], _State) when is_float(Coef) ->
	NormCoef = max(min(Coef, 1.0), 0.0) * ?GP_IMPULSE_PLAYER_COEF,
	Action = gp_action:new(action_push, [{coef, NormCoef}]),
	gp_world:user_action(self(), Action);

json_msg_handle_impl(_Msg, _State) ->
	ok.

%% ------------------------------------
%% binary_msg_handle_impl
%% ------------------------------------

-spec binary_msg_handle_impl(
	binary()
) -> ok.

binary_msg_handle_impl(_Msg) ->
	ok.

%% ------------------------------------
%% say_impl
%% ------------------------------------

-spec say_impl(
	pid(), binary()
) -> ok.

say_impl(User, Msg) ->
	UserId = gp_world:get_user_id(User),
	lager:info("Say: Id:~p Msg:~ts~n", [UserId, Msg]),
	lists:foreach(fun(OtherUser) ->
		send_json(OtherUser, <<"chat">>, [UserId, Msg])
	end, gp_world:get_all_users()).
