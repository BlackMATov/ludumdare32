%% ----------------------------------------------------------------------------
%%
%% DEFINES
%%
%% ----------------------------------------------------------------------------

-define(GP_TICK_INTERVAL,          100).
-define(GP_STATS_INTERVAL,         30000).
-define(GP_PIANO_KILLS_COEF,       0.2).
-define(GP_PIANO_WIDTH,            120).
-define(GP_PIANO_XOFFSET,          100).
-define(GP_PIANO_YOFFSET,          100).
-define(GP_PIANO_SPAWN_OFFSET,     650).
-define(GP_PIANO_COOLDOWN,         1000).
-define(GP_PIANO_PUSH_ZONE,        10).
-define(GP_PLAYER_SPEED,           7).
-define(GP_PLAYER_ACT_TIME,        500).
-define(GP_PLAYER_SPAWN_MIN,       30).
-define(GP_PLAYER_FRAG_SCORE,      5).
-define(GP_ZOMBIE_SPEED,           2).
-define(GP_ZOMBIE_MAX_COEF,        20).
-define(GP_ZOMBIE_MAX_HEALTH,      3000).
-define(GP_ZOMBIE_MAX_HEALTH_COEF, 0.1).
-define(GP_IMPULSE_MAX,            20.0).
-define(GP_IMPULSE_PLAYER_COEF,    6.0).
-define(GP_IMPULSE_PLAYER_STATIC,  1.0).
-define(GP_IMPULSE_ZOMBIE_STATIC,  0.5).
-define(GP_IMPULSE_COOLDOWN,       200).

%% ----------------------------------------------------------------------------
%%
%% TYPES
%%
%% ----------------------------------------------------------------------------

-type gp_json()   :: list() | tuple() | number() | binary().
-type gp_vec2()   :: gp_vec2:vec2().
-type gp_entity() :: ees:entity().

-type gp_comp_type() ::
	player |
	piano |
	zombie.

-type gp_com_state() ::
	gp_player_comp() |
	gp_piano_comp() |
	gp_zombie_comp().

%% ----------------------------------------------------------------------------
%%
%% COMPONENTS
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% gp_player_comp
%% ------------------------------------

-record(gp_player_comp, {
	pos    = {0,0}           :: gp_vec2(),
	dir    = 1               :: integer(),
	name   = <<"anonymous">> :: binary(),
	view   = 0               :: integer(),
	score  = 0               :: integer(),
	user   = undefined       :: pid() | undefined,
	action = undefined       :: gp_action:state() | undefined
}).
-type gp_player_comp() :: #gp_player_comp{}.

%% ------------------------------------
%% gp_piano_comp
%% ------------------------------------

-record(gp_piano_comp, {
	pos              = {0,0} :: gp_vec2(),
	sound            = 0     :: integer(),
	sound_rnd        = 0     :: integer(),
	cooldown         = 0     :: integer(),
	frags            = 0     :: integer(),
	impulse          = 0.0   :: float(),
	impulse_cooldown = 0     :: integer(),
	spawn_cooldown   = 0     :: integer()
}).
-type gp_piano_comp() :: #gp_piano_comp{}.

%% ------------------------------------
%% gp_zombie_comp
%% ------------------------------------

-record(gp_zombie_comp, {
	pos    = {0,0} :: gp_vec2(),
	view   = 0     :: integer(),
	health = 0.0   :: float()
}).
-type gp_zombie_comp() :: #gp_zombie_comp{}.
