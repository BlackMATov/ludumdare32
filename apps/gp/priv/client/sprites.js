var sprites = {};


sprites.player = [
	[
		{
			file:"art/player1.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player1.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player1.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player2.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player2.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player2.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player3.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player3.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player3.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player4.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player4.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player4.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player5.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player5.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player5.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player6.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player6.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player6.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player8.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player8.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player8.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player9.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player9.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player9.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player10.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player10.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player10.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player11.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player11.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player11.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 0,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 32,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 64,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 96,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 128,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 160,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 96,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 128,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 160,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 192,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 224,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 256,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 192,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 224,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 256,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 288,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 320,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 352,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player12.png",
			s: 288,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 320,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player12.png",
			s: 352,
			t: 192,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player13.png",
			s: 0,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 32,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 64,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],	
	[
		{
			file:"art/player13.png",
			s: 96,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 128,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 160,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player13.png",
			s: 192,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 224,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 256,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/player13.png",
			s: 288,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 320,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/player13.png",
			s: 352,
			t: 64,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
];



sprites.zombie = [
	[
		{
			file:"art/ZombieSheet.png",
			s: 0,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 32,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 64,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/ZombieSheet.png",
			s: 96,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 96+32,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 96+64,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/ZombieSheet.png",
			s: 192+0,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 192+32,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 192+64,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/ZombieSheet.png",
			s: 288+0,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 288+32,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 288+64,
			t: 32,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],


	[
		{
			file:"art/ZombieSheet.png",
			s: 0,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 32,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 64,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/ZombieSheet.png",
			s: 96,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 96+32,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 96+64,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/ZombieSheet.png",
			s: 192+0,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 192+32,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 192+64,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	],
	[
		{
			file:"art/ZombieSheet.png",
			s: 288+0,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 288+32,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		},
		{
			file:"art/ZombieSheet.png",
			s: 288+64,
			t: 160,
			width: 32,
			height: 32,
			ofs_x: 16,
			ofs_y: 30
		}
	]
];



sprites.back =
[
	{
		file:"art/back.png"
	}
];



sprites.shadow =
{
	file:"art/shadow.png",
	ofs_x: 10,
	ofs_y: 5
};



sprites.piano_shadow =
{
	file:"art/piano_shadow.png",
	ofs_x: -2,
	ofs_y: -80
};

sprites.blood =
[
	{
		file:"art/blood1.png",
		ofs_x: 24,
		ofs_y: 34
	},
	{
		file:"art/blood2.png",
		ofs_x: 24,
		ofs_y: 34
	},
	{
		file:"art/blood3.png",
		ofs_x: 24,
		ofs_y: 34
	},
	{
		file:"art/blood4.png",
		ofs_x: 24,
		ofs_y: 34
	},
	{
		file:"art/blood5.png",
		ofs_x: 24,
		ofs_y: 34
	},
	{
		file:"art/blood6.png",
		ofs_x: 24,
		ofs_y: 34
	},
];

sprites.piano =
[
	{
		file:"art/lid1.png",
		ofs_x: 0,
		ofs_y: 48,
		width: 118,
		height: 150
	},
	{
		file:"art/lid2.png",
		ofs_x: 0,
		ofs_y: 48,
		width: 118,
		height: 150
	},
	{
		file:"art/lid3.png",
		ofs_x: 0,
		ofs_y: 48,
		width: 118,
		height: 150
	},
];



sprites.lantern = {
	file:"art/lamp.png",
	ofs_x: 26
};

