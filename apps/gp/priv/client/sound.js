var audio_context;
var volume = 1.0;


var sounds = {
	piano:[],
	zombie:[],
	lid:[]
};

function init_sound ()
{
	window.AudioContext = window.AudioContext || window.webkitAudioContext;
	audio_context = new AudioContext();

	var loader = new BufferLoader(
		audio_context,
		[
			'audio/piano1.mp3',
			'audio/piano2.mp3',
			'audio/piano3.mp3',
			'audio/Zombie_1.mp3',
			'audio/Zombie_2.mp3',
			'audio/Zombie_Dead_1.mp3',
			'audio/Zombie_Dead_2.mp3'
		],
		function (buf_list) {
			console.log('sound loaded');
			sounds.piano = [
				buf_list[0],
				buf_list[1],
				buf_list[2]
			];
			sounds.zombie = [
				buf_list[3],
				buf_list[4]
			];
			sounds.lid = [
				buf_list[5],
				buf_list[6]
			];

		var vol_elt = document.getElementById('volume');
		vol_elt.onchange = function () {
			volume = vol_elt.value / 100;
			if (bgm) bgm_gain.gain.value = volume * bgm_volume;
		}
		vol_elt.onchange ();

		init_bgm ();

		setTimeout (play_ambient, 5000);
	});

	loader.load ();
}



function play_sound_simple (buffer, vol)
{
	if (!buffer) return;
	var source = audio_context.createBufferSource();
	var gain = audio_context.createGain();
	source.buffer = buffer;
	source.connect (gain);
	gain.connect (audio_context.destination);
	gain.gain.value = volume * (vol || 1);
	source.start (0);
	return source;
}



function play_sound_pitched (buffer, pitch, vol)
{
	if (!buffer) return;
	var source = audio_context.createBufferSource();
	var compressor = audio_context.createDynamicsCompressor();
	var gain = audio_context.createGain();
	source.buffer = buffer;
	source.connect (gain);
	gain.connect (compressor);
	compressor.connect (audio_context.destination);
	gain.gain.value = volume * (vol || 1);
	//source.playbackRate.value = pitch;
	source.start (0);
	return source;
}


var music;
var bgm, bgm_gain;
var bgm_volume = 0.5;
function init_bgm ()
{
	var tracks = [
		'audio/OST_2_-_Loop_1.mp3',
		'audio/OST_2_-_Loop_2.mp3'
	];

	var loader = new BufferLoader(
		audio_context,
		[tracks[(rng() * tracks.length) | 0]],
		function (buf_list) {
			console.log('bgm loaded');
			music = buf_list[0];

			bgm = audio_context.createBufferSource();
			bgm_gain = audio_context.createGain();
			bgm.buffer = music;
			bgm.connect (bgm_gain);
			bgm_gain.connect (audio_context.destination);
			bgm_gain.gain.value = volume * bgm_volume;
			bgm.start (0);

			bgm.loop = true;
	});

	loader.load ();
}



function play_ambient ()
{
	var z = (sounds.zombie.length * rng()) | 0;
	play_sound_simple (sounds.zombie[z], 8);
	setTimeout (play_ambient, 2000 + (10000 * rng()) | 0);
}

