function Client() {
	this._socket         = undefined;
	this._online         = false;
	this._handlers       = [];
	this._sps            = 0; // send per second
	this._sps_count      = 0;
	this._rps            = 0; // receive per secodn
	this._rps_count      = 0;
	this._sbps           = 0; // send bytes per second
	this._sbps_count     = 0;
	this._rbps           = 0; // receive bytes per second
	this._rbps_count     = 0;
	this._stats_interval = null;
};

Client.prototype.start = function(start_func, finish_func) {
	this._socket = new WebSocket("ws://" + window.location.host + "/websocket");
	this._socket.binaryType = "arraybuffer";
	
	this._socket.onopen = function() {
		this._online = true;
		this._stats_interval = setInterval(function() {
			this._sps = this._sps_count;
			this._sps_count = 0;
			this._rps = this._rps_count;
			this._rps_count = 0;
			this._sbps = this._sbps_count;
			this._sbps_count = 0;
			this._rbps = this._rbps_count;
			this._rbps_count = 0;
		}.bind(this), 1000);
		if ( start_func ) {
			start_func();
		}
	}.bind(this);
	
	this._socket.onclose = function() {
		if ( this.is_online() ) {
			this._online = false;
			clearInterval(this._stats_interval);
			this._stats_interval = null;
			if ( finish_func ) {
				finish_func();
			}
		}
	}.bind(this);
	
	this._socket.onmessage = function(evt) {
		var msg = null;
		try {
			if ( typeof evt.data == "string" ) {
				this._rps_count++;
				this._rbps_count += evt.data.length;
				msg = JSON.parse(evt.data);
			} else {
				this._rps_count++;
				this._rbps_count += evt.data.byteLength;
				msg = JSON.parse((function(buf) {
					var ret = "";
					var arr = new Uint8Array(buf);
					for ( var i = 0; i < arr.length; ++i ) {
						ret += String.fromCharCode(arr[i]);
					}
					return ret;
				})(evt.data));
			}
		} catch ( e ) {
			console.error("client.onmessage parse error");
		}
		if ( msg ) {
			var msg_id    = msg[0];
			var msg_value = msg[1] || [];
			for ( var i = 0; i < this._handlers.length; ++i ) {
				var handler = this._handlers[i];
				if ( handler.id === "" || handler.id === msg_id ) {
					handler.func(msg_value);
					if ( handler.once ) {
						this._handlers.splice(i, 1);
						--i;
					}
				}
			}
		}
	}.bind(this);
};

Client.prototype.is_online = function() {
	return this._online;
};

Client.prototype.is_offline = function() {
	return !this._online;
};

Client.prototype.get_status = function() {
	return this.is_online() ? "online" : "offline";
};

Client.prototype.get_sps = function() {
	return this._sps;
};

Client.prototype.get_rps = function() {
	return this._rps;
};

Client.prototype.get_sbps = function() {
	return this._sbps;
};

Client.prototype.get_rbps = function() {
	return this._rbps;
};

Client.prototype.add_handler = function(msg_id, handler, once) {
	this._handlers.push({
		id   : msg_id,
		func : handler,
		once : once || false
	});
};

Client.prototype.send = function(msg_id, value) {
	if ( this.is_online() ) {
		var msg = [msg_id, value || []];
		var str = JSON.stringify(msg);
		this._socket.send(str);
		this._sps_count++;
		this._sbps_count += str.length;
	}
};

Client.prototype.request = function(request, value, func) {
	if ( this.is_online() ) {
		this.add_handler(request + "_info", function(value) {
			func(value);
		}, true);
		this.send("request_" + request, value);
	}
};
