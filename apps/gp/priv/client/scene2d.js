function Scene2D (canvas, ctx)
{
	this.src_path = "";
	this.canvas = canvas;
	this.ctx = ctx;
	this.image_pool = {};
	this.spr_array = [];
	this.loading_images = 0;
	this.offset_x = 0;
	this.offset_y = 0;
}

Scene2D.prototype.load_one_sprite = function(spr)
{
	if(spr.s == undefined) spr.s = 0;
	if(spr.t == undefined) spr.t = 0;
	if(spr.ofs_x == undefined) spr.ofs_x = 0;
	if(spr.ofs_y == undefined) spr.ofs_y = 0;

	if(this.image_pool[spr.file] == undefined) {
		spr.image = new Image();
		var self = this;
		spr.image.onload = function() {
			if(spr.width == undefined) spr.width = this.width;
			if(spr.height == undefined) spr.height = this.height;
			--self.loading_images;
			spr.image.is_loading = false;
		}
		++this.loading_images;
		spr.image.is_loading = true;
		spr.image.src = this.src_path + spr.file;
		this.image_pool[spr.file] = spr.image;
	} else {
		spr.image = this.image_pool[spr.file];
	}
}

Scene2D.prototype.load_sprite = function(spr)
{
	if(spr.file == undefined) {
		for (var k in spr) this.load_sprite(spr[k]);
	} else {
		this.load_one_sprite(spr);
	}
}

Scene2D.prototype.draw_sprite = function(img,spr,x,y,z,mirror)
{
	this.spr_array.push([z||0,img,spr,x|0,y|0,mirror||0]);
}

Scene2D.prototype.begin_render = function()
{
	this.ctx.clearRect (0, 0, this.canvas.width, this.canvas.height);
	this.ctx.translate (this.offset_x, this.offset_y);
}

Scene2D.prototype.render_sprites = function()
{
	if(!this.spr_array.length) return;
	this.spr_array.sort (function (a, b) {return a[0] - b[0];});
	var i = this.spr_array.length;
	var s, spr, img;
	var ctx = this.ctx;
	while(i--) {
		s = this.spr_array[i];
		img = s[1];
		spr = s[2];
		ctx.save ();
		ctx.translate (s[3], s[4]);
		if (s[5]) ctx.scale (-1, 1);
		ctx.drawImage (img, spr.s, spr.t, spr.width, spr.height, -spr.ofs_x, -spr.ofs_y, spr.width, spr.height);
		ctx.restore ();
	}
	this.spr_array = [];
}

Scene2D.prototype.finish_render = function()
{
	this.ctx.translate (-this.offset_x, -this.offset_y);
}
