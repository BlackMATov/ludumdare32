var game_time = 0;
var player_id = -1;

var players = {};
var zombies = {};
var piano, pianos = {};
var piano_serv_cooldown = 0, piano_prev_cooldown = 0;

var blood = [];

var screen_y0 = 300 - 100 - 24;


function parse_movable (data, mobs)
{
	var i, p;
	var id;
	for (i in data) {
		p = data[i];
		id = p[0];

		// add new mob
		pl = mobs[id];
		if (!pl) {
			pl = {
				id: id,
				x: parseInt(p[1][0]),
				y: parseInt(p[1][1])
			};
			mobs[id] = pl;
		}

		// update mob data
		pl.x1 = parseInt(p[1][0]);
		pl.y1 = parseInt(p[1][1]);
		pl.ts = game_time;
	}
}



function drop_old (mobs, f)
{
	var i;
	for (i in mobs) {
		if (mobs[i].ts != game_time) {
			if (f) f (mobs[i]);
			delete mobs[i];
		}
	}
}



function parse_players (data)
{
	parse_movable (data, players);

	var i, p;
	var id;
	for (i in data) {
		p = data[i];
		id = p[0];
		pl = players[id];

		// update player data
		pl.anim = pl.anim || 0;
		pl.dir = parseInt(p[2]);
		pl.name = p[3];
		pl.view = parseInt(p[4]);
		pl.score = parseInt(p[5]);
	}

	drop_old (players);
}



function parse_zombies (data)
{
	parse_movable (data, zombies);

	var i, p;
	var id;
	for (i in data) {
		p = data[i];
		id = p[0];
		pl = zombies[id];

		pl.view = parseInt(p[2]);
	}

	var kills = 0;
	drop_old (zombies, function (mob) {
		var b = {
			x: mob.x,
			y: mob.y,
			t0: game_time + (30 * rng()) | 0
		};
		blood.push (b);
		kills++;
	});

	if (kills) {
		setTimeout (function () {
			play_sound_pitched (sounds.lid[(rng() * sounds.lid.length) | 0], 1 + 0.25 * rng(), 2);
		}, 300);
	}
}



function parse_piano (data)
{
	parse_movable (data, pianos);

	var i, p, c;
	var id;
	for (i in data) {
		p = data[i];
		id = p[0];
		pl = pianos[id];

		// update piano data
		c = parseInt(p[4]);
		if (!(pl.cooldown > c)) {
			pl.cooldown = c;
			pl.phase = c - game_time;
			piano_serv_cooldown = c;
		}
		pl.sound = parseInt(p[2]);
		pl.pitch = parseInt(p[3]);
		pl.frags = parseInt(p[5]);

		piano = pl;
	}
}



function world_handler (data)
{
	game_time = parseInt(data[3]);
	parse_players (data[0]);
	parse_zombies (data[1]);
	parse_piano (data[2]);
}



function adjust_movable (mobs)
{
	var i, pl;
	for (i in mobs) {
		pl = mobs[i];
		if (pl.x != pl.x1) {
			var dif = Math.abs(pl.x1 - pl.x);
			if (dif > 30 || dif < 3) {
				pl.x = pl.x1;
			} else {
				if (pl.x1 > pl.x) pl.x += 2;
				else pl.x -= 2;
			}
			pl.anim++;
		}
	}
}



function zombie_spr (pl)
{
	var set = sprites.zombie[pl.view];
	var w = pl.id + now() / 400;
	w = Math.floor(w) % set.length;
	return set[w];
}



function player_spr (pl)
{
	var set = sprites.player[pl.view];
	var w = ((pl.anim / 5) | 0) % set.length;
	return set[w];
}



function adjust_piano ()
{
	piano.phase -= 16;
	if (piano.phase < 0) piano.phase = 0;
}



function draw_movable (mobs, sprf)
{
	var i, pl, spr;
	for (i in mobs) {
		pl = mobs[i];
		spr = sprf(pl);
		scene.draw_sprite (spr.image, spr, pl.x, screen_y0 + pl.y, -pl.y, pl.dir < 0);
		scene.draw_sprite (sprites.shadow.image, sprites.shadow, pl.x, screen_y0 + pl.y, -pl.y - 0.1);
	}
}



function draw_names (mobs)
{
	var ctx = canvas.main.ctx;
	ctx.fillStyle = 'white';
	ctx.font = "10px Courier";
	ctx.textAlign = "center";

	function draw_name_plate (pl)
	{
		ctx.fillText (pl.name, pl.x, screen_y0 + pl.y + 10);
		ctx.fillText ('<' + pl.score + '>', pl.x, screen_y0 + pl.y + 22);
	}

	var i;
	for (i in players) {
		if (i == player_id) continue;
		draw_name_plate (players[i]);
	}

	ctx.fillStyle = '#E0E0FF';
	draw_name_plate (players[player_id]);
}



function draw_piano (mobs, sprf)
{
	var spr;
	var t = piano.phase / 1000;
	if (t == 0) spr = sprites.piano[0];
	else spr = sprites.piano[(t * sprites.piano.length) | 0];

	scene.draw_sprite (spr.image, spr, piano.x, screen_y0 + piano.y, -piano.y);
	scene.draw_sprite (sprites.piano_shadow.image, sprites.piano_shadow, piano.x, screen_y0 + piano.y, -piano.y - 10.1);
}



function draw_blood ()
{
	var b, f, spr;
	var i = blood.length;
	while (--i >= 0) {
		b = blood[i];
		f = ((game_time - b.t0) / 60) | 0;
		if (f >= sprites.blood.length) {
			blood.splice (i, 1);
			continue;
		}
		spr = sprites.blood[f];
		scene.draw_sprite (spr.image, spr, b.x, screen_y0 + b.y, -b.y);
	}
}



function draw_background ()
{
	var bg = sprites.back[0];
	var x = ((-scene.offset_x / bg.width) | 0) * bg.width;
	var w;
	for (w = canvas.main.width; w > 0; w -= bg.width) {
		scene.draw_sprite (bg.image, bg, x, 0, 10000);
		x += bg.width;
	}
}



function adjust_camera ()
{
	scene.offset_x = -players[player_id].x + canvas.main.width / 2;
	if (scene.offset_x > 0) scene.offset_x = 0;
}



function update_buttons_state ()
{
	var top_btn = document.getElementById('ctrl_top');
	var right_btn = document.getElementById('ctrl_right');
	var push_btn = document.getElementById('ctrl_push');

	var near = players[player_id].x > piano.x - 10;

	if (piano.cooldown > game_time || !near) top_btn.disabled = true;
	else top_btn.disabled = false;

	if (near) {
		right_btn.style.display = 'none';
		push_btn.style.display = 'block';
	} else {
		right_btn.style.display = 'block';
		push_btn.style.display = 'none';
	}
}



function draw_progress ()
{
	var ctx = canvas.main.ctx;
	ctx.font = "16px courier";
	ctx.fillStyle = "yellow";
	ctx.textAlign = "right";
	ctx.fillText ("Grand piano dragged for " + (piano.x / 10).toFixed(1) + "m", canvas.main.width - 10, screen_y0 + piano.y + 115);
	ctx.font = "bold 16px courier";
	ctx.fillStyle = "red";
	ctx.fillText ("Zombies killed " + piano.frags, canvas.main.width - 10, screen_y0 + piano.y + 115 - 20);
}



function draw_top_players ()
{
	var top = [];
	var i, l, html = '', pl;
	for (i in players) top.push (players[i]);

	top.sort (function (a, b) { return b.score - a.score; });

	for (i = 0; i < top.length; i++) {
		pl = top[i];
		l = '<span class="score">' + pl.score + '</span>' + pl.name;
		if (pl.id == player_id) l = '<span class="mychat">' + l + '</span>';
		html += l + '<br>\n';
	}
	document.getElementById('topplayers').innerHTML = html;
}



function draw_lanterns ()
{
	var spr = sprites.lantern;
	var l = 400;
	var x0 = (((-scene.offset_x / l) | 0) - 1) * l;
	var i, x, dx;

	for (i = 0; i < 10; i++) {
		x = x0 + i * l;
		dx = (x + scene.offset_x) * 1.5;
		x = -scene.offset_x + dx;
		scene.draw_sprite (spr.image, spr, x, 0, 1000);
	}
}



function tick ()
{
	adjust_movable (players);
	adjust_movable (zombies);
	adjust_movable (pianos);
	adjust_piano ();
	adjust_camera ();

	update_buttons_state ();

	scene.begin_render ();
	draw_background ();
	draw_movable (players, player_spr);
	draw_movable (zombies, zombie_spr);
	draw_piano ();
	draw_blood ();
	draw_lanterns ();
	scene.render_sprites ();
	draw_names ();
	scene.finish_render ();
	draw_progress ();

	if (piano_prev_cooldown != piano_serv_cooldown) {
		play_sound_pitched (sounds.piano[piano.sound], 1 + piano.pitch / 1000);
		piano_prev_cooldown = piano_serv_cooldown;
	}

	window.requestAnimationFrame (tick);
}

