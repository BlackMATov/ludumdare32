var player_name;



function random_name ()
{
	var predef = ['Matvey', 'Pavel', 'Ilya', 'Sergey'];
	return predef[(predef.length * rng()) | 0];
}



function init_name ()
{
	var name = localStorage.getObject('player');
	if (name) {
		player_name = name.name;
	} else {
		player_name = random_name();
	}

	localStorage.setObject('player', {name:player_name});
	net.send ('change_name', [player_name]);

	var name_elt = document.getElementById('name');
	name_elt.value = player_name;
	name_elt.onchange = function () {
		player_name = name_elt.value;
		localStorage.setObject('player', {name:player_name});
		net.send ('change_name', [player_name]);
	}
}



function init_chat ()
{
	function say ()
	{
		if (say_elt.value == '') return;
		net.send ('say', [say_elt.value]);
		say_elt.value = '';
	}

	var say_elt = document.getElementById('say');
	var say_btn = document.getElementById('say_btn');
	say_btn.onclick = say;
	say_elt.addEventListener("keypress", function (e) {
		if (e.keyCode == 13) say();
	});

	document.getElementById('chat').style.display = 'block';
	net.add_handler ('chat', recv_messages);
}


function recv_messages (m)
{
	var id = parseInt(m[0]);
	var name = players[id].name;
	if (id == player_id) name = '<span class="mychat">' + name + '</span>';
	var html = name + ': ' + m[1] + '<br>\n';
	var messages = document.getElementById('messages');
	messages.innerHTML += html;
	messages.scrollTop = messages.scrollHeight;
}
